var express = require("express")
var connect = require("connect")
var auth = require("./auth")
var app = express()
var path = require("path")

app.configure(function() {
  app.use(express.logger())
  app.use(connect.compress())
  app.use(express.cookieParser())
  app.use(express.bodyParser())
  app.use(express.session({secret: "roomapplication session"}))
  app.use(auth.initialize())
  app.use(auth.session())
})

const INDEX_HTML = `index.html`

app.get("/saml/home", auth.protected, function(req, res) {
  res.sendfile(INDEX_HTML)
})

//auth.authenticate check if you are logged in
app.get(
  "/saml/login",
  auth.authenticate("saml", {failureRedirect: "/", failureFlash: true}),
  function(req, res) {
    res.redirect("/")
  }
)

//POST Methods, redirect to home successful login
app.post(
  "/saml/login",
  auth.authenticate("saml", {failureRedirect: "/", failureFlash: true}),
  function(req, res) {
    res.redirect("/saml/home")
  }
)

//code for importing static files
app.use(express.static(path.join(__dirname, "public")))
var currentPort = app.listen(process.env.PORT || 3000)
console.log("Server started at PORT " + currentPort)
